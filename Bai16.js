function listProduct(){
    return products = [
        {"id":"1", "name":"Phone", "categoryId":"3","saleDate":"5/4/2022","qulity":2,"isDelete":"false"},
        {"id":"2", "name":"Phone", "categoryId":"2","saleDate":"5/4/2022","qulity":2,"isDelete":"false"},
        {"id":"3", "name":"Phone", "categoryId":"2","saleDate":"5/4/2022","qulity":2,"isDelete":"false"},
        {"id":"4", "name":"Phone", "categoryId":"2","saleDate":"5/4/2022","qulity":2,"isDelete":"false"},
        {"id":"5", "name":"Phone1", "categoryId":"2","saleDate":"5/4/2022","qulity":2,"isDelete":"true"},
    ]
}

function filterBySaleDate(listProduct){
    var list =listProduct.filter((product) =>  product.qulity > 0 && Date.parse(product.saleDate) > Date.now());
    return list.map((product) => [product.id,product.name]);
}
console.log(filterBySaleDate(listProduct()));

// cách dùng for
function filterBySaleDate1(listProduct)
{   
    let arrPro = [];
    for (let index = 0; index < listProduct.length; index++) {
        if ( listProduct[index].qulity > 0 && Date.parse(listProduct[index].saleDate) > Date.now()) {
             arrPro.push([listProduct[index].id, listProduct[index].name ]); 
        }
    }
    return arrPro;
}
console.log(filterBySaleDate1(listProduct()));


